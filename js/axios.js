//AXIOS
const BASE_URL = "https://633ec04783f50e9ba3b75fed.mockapi.io";

export let getListProducts = () => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser`,
    method: "GET",
  });
};

export let getProductsId = (id) => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser/${id}`,
    method: "GET",
  });
};
