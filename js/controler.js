// LOADING
export let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
export let offLoading = () => {
  document.getElementById("loading").style.display = "none";
};
//RENDER
export let renderProducts = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    contentHTML += `<div class="product-box">
    <img
      src="${item.img}"
      alt=""
      class="product-img"
    />
    <h2 class="product-title">${item.name}</h2>
    <span class="price">${item.price}</span>
    <i class="bx bx-cart add-cart"></i>
    </div>
  `;
  });
  document.getElementById("showBodyProducts").innerHTML = contentHTML;
};
