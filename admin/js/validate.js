export let test = (value, idError) => {
    if (value.length == 0) {
        document.getElementById(idError).innerText =
            'Trường này không được để trống';
        return false;
    } else {
        document.getElementById(idError).innerText = '';
        return true;
    }
};
export let testNull = (dataTest) => {
    let isValid = true;
    isValid =
        test(dataTest.name, 'spanName') &
        test(dataTest.price, 'spanPrice') &
        test(dataTest.screen, 'spanScreen') &
        test(dataTest.backCamera, 'spanBackCamera') &
        test(dataTest.frontCamera, 'spanFrontCamera') &
        test(dataTest.img, 'spanImg') &
        test(dataTest.desc, 'spanDesc') &
        test(dataTest.type, 'spanType');
    return isValid;
};
