//AXIOS
const BASE_URL = "https://633ec04783f50e9ba3b75fed.mockapi.io";
// capstoneJsUser
export let getListProducts = () => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser`,
    method: "GET",
  });
};

export let getProductsId = (id) => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser/${id}`,
    method: "GET",
  });
};

export let postCreateProducts = (element) => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser`,
    method: "POST",
    data: element,
  });
};

export let deleteProductsByIdAxios = (id) => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser/${id}`,
    method: "DELETE",
  });
};

export let putProductsId = (id, element) => {
  return axios({
    url: `${BASE_URL}/capstoneJsUser/${id}`,
    method: "PUT",
    data: element,
  });
};
