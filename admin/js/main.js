import { renderProducts, getValueInput, showInformation } from './controler.js';
import { products, productsNull } from './model.js';
import {
    getListProducts,
    postCreateProducts,
    deleteProductsByIdAxios,
    getProductsId,
    putProductsId,
} from './axios.js';
import { testNull } from './validate.js';
// lần đầu user load trang
let renderListProducts = () => {
    getListProducts()
        .then((res) => {
            //convert
            let listProducts = res.data.map((item) => {
                return new products(
                    item.name,
                    item.price,
                    item.screen,
                    item.backCamera,
                    item.frontCamera,
                    item.img,
                    item.desc,
                    item.type,
                    item.id
                );
            });
            renderProducts(listProducts);
        })
        .catch((err) => {
            Swal.fire('lấy dữ liệu thất bại');
        });
};
renderListProducts();
// ADD
document
    .getElementById('btnAddProducts')
    .addEventListener('click', function () {
        let dataform = getValueInput();
        if (testNull(dataform)) {
            postCreateProducts(dataform)
                .then((res) => {
                    Swal.fire('Thêm thành công');
                    renderListProducts();
                    showInformation(productsNull);
                })
                .catch((err) => {
                    Swal.fire('Thêm thất bại');
                });
        }
    });
// DELETE
let deleteProducts = (idProducts) => {
    deleteProductsByIdAxios(idProducts)
        .then((res) => {
            renderListProducts();
            Swal.fire('Xóa thành công');
        })
        .catch((err) => {
            Swal.fire('Xóa thất bại');
        });
};
window.deleteProducts = deleteProducts;
// REPAIR
let idArray = [];

let getDetailProducts = (idProducts) => {
    getProductsId(idProducts)
        .then((res) => {
            console.log('res: REPAIR', res);
            showInformation(res.data);
            idArray.push(res.data.id);
            return idArray, console.log('idArray: ', idArray);
        })
        .catch((err) => {
            console.log('err: ', err);
        });
};
window.getDetailProducts = getDetailProducts;
// UPDATE
document.getElementById('btnRepair').addEventListener('click', function () {
    let dataform = getValueInput();
    let idPopArray = idArray.pop();
    if (testNull(dataform)) {
        putProductsId(idPopArray, dataform)
            .then((res) => {
                Swal.fire('Cập nhật thành công');
                renderListProducts();
                showInformation(productsNull);
            })
            .catch((err) => {
                Swal.fire('Cập nhật thất bại');
            });
    }
});

// RESET
let resetForm = document
    .getElementById('btnReset')
    .addEventListener('click', function () {
        showInformation(productsNull);
    });

// TEST NULL
